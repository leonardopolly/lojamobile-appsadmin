const mysql = require('mysql');

const query = (query, cb) => {
  const connection = mysql.createConnection({
    host     : '127.0.0.1',
    user     : 'root',
    password : '',
    database : 'appsadmin'
  });

  connection.connect();
  connection.query(query, cb);
  connection.end();
};


module.exports = {
  getApps: (req, res) => {
    query(
      'SELECT * FROM apps', 
      (error, results, fields) => {
        if (error) throw error;
        // console.log(results);
        res.send(results);
      }
    );
  },
  getApp: (req, res) => {
    query(
      `SELECT * FROM apps WHERE id = ${ req.params.id }`, 
      (error, results, fields) => {
        if (error) throw error;
        // console.log(results[0]);
        res.send(results[0]);
      }
    );
  },
  insertApp: (req, res) => {
    const app = {icon: req.file && req.file.path, ...req.body};

    query(
      `INSERT INTO \`apps\` (${ Object.keys(app).map((k)=>`\`${k}\``).join(',') }) VALUES (${ Object.values(app).map(v => `'${v}'`).join(',') })`,
      (error, results, fields) => {
        if (error) throw error;
        // console.log(results);
        res.send(results);
      }
    );
  },
  updateApp: (req, res) => {
    const app = {icon: req.file && req.file.path, ...req.body};

    query(
      `UPDATE \`apps\` 
        SET
          ${
            Object.keys(app).map((k) => `${k} = '${app[k] ? app[k] : ''}'`).join(',')
          }
        WHERE
          id = ${req.params.id}`,
      (error, results, fields) => {
        if (error) throw error;
        // console.log(results);
        res.send(results);
      }
    );
  },
  deleteApp: (req, res) => {
    query(
      `DELETE FROM apps WHERE id = ${req.params.id}`,
      (error, results, fields) => {
        if (error) throw error;
        // console.log(results);
        res.send(results);
      }
    );
  },
};