import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";

const Page = ({ title, component, children, ...rest }) => {
  const [pageTitle] = useState(title);

  useEffect(() => {
    const standardTitle = "LojaMobile Admin";
    document.title = pageTitle
      ? `${pageTitle} - ${standardTitle}`
      : standardTitle;
  });

  return (
    children ? (
      <Route {...rest}>
        {children}
      </Route>
    ) : (
      <Route
        {...rest}
        component={ component }
      />
    )
  );
};

Page.propTypes = {
  title: PropTypes.string,
  component: PropTypes.func,
  children: PropTypes.node
};

export default Page;
