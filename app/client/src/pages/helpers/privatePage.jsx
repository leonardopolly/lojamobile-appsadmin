import React from "react";
import { Redirect } from "react-router-dom";
import Page from "./page";

const PrivatePage = props => {
  return localStorage.getItem("authToken") ? (
    <Page {...props} />
  ) : (
    <Redirect
      to={{
        pathname: "/login"
      }}
    />
  );
};

export default PrivatePage;
