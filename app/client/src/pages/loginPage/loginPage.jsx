import React from "react";
import { useHistory } from "react-router-dom";
import useLoginForm from "./hooks/useLoginForm";

import { makeStyles } from '@material-ui/core/styles';
import { Container, TextField, Box, Button, Grid, Paper } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  loginActions: {
    marginTop: theme.spacing(2)
  }
}));

const LoginPage = () => {
  const history = useHistory();
  const classes = useStyles();

  const { inputs, handleInputChange, handleSubmit } = useLoginForm(
    { user: "", pass: "" },
    () => {
      localStorage.setItem("user", inputs.user);
      localStorage.setItem("authToken", btoa(inputs.pass)); // apenas para testes

      history.replace("/");
    }
  );

  return (
    <Container maxWidth="xs">
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        style={{ minHeight: '100vh' }}
      >
        <Grid item>
          <Paper elevation={3}>
            <form onSubmit={handleSubmit} action="">
              <Box p={3}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      id="user"
                      name="user" 
                      label="Usuário"
                      value={inputs.user}
                      onChange={handleInputChange}
                      required 
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      type="password"
                      name="pass"
                      id="pass"
                      label="Senha"
                      value={inputs.pass}
                      onChange={handleInputChange}
                      required 
                    />
                  </Grid>
                  <Grid item xs={12} className={classes.loginActions}>
                    <Button type="submit" variant="contained" color="primary">
                      Entrar
                    </Button>
                  </Grid>
                </Grid>
              </Box>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default LoginPage;
