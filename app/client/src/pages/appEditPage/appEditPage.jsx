import React, { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import axios from 'axios';
import { AppForm } from '~/components';



const AppEditPage = () => {
  const { id } = useParams();

  const [initialValues, setInitialValues] = useState({ 
    icon: '',
    name: '',
    version: '',
    minimumVersion: '',
    appUrl: '',
    appCreator: '',
    platform: '',
    profiles: [],
  })

  useEffect(() => {
    (async () => {
      const { profiles, ...rest } = await axios(`/api/apps/${id}`).then(response => response.data);
      const initialValues = { 
        ...rest, 
        profiles: profiles? profiles.split(',') : []
      }
      
      setInitialValues(initialValues);
    })();
  }, [id])

  return ( 
    <AppForm initialValues={ initialValues } />
  );
}
 
export default AppEditPage;