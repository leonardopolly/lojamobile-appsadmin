import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { 
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Avatar, 
  IconButton, 
  Typography,
  Toolbar,
  Button
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Delete as DeleteIcon
} from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  title: {
    flexGrow: 1
  }
});

const AppsPage = () => {
  const classes = useStyles();
  const [apps,setApps] = useState([]);
  const history = useHistory();

  useEffect(() => {
    (async () => {
      const rows = await axios('/api/apps').then(response => response.data);
      setApps(rows);
    })();
  }, []);

  const removeApp = async (id) => {
    const remainingApps = apps.filter((app) => app.id !== id);
    await axios.delete(`/api/apps/${id}`);
    setApps(remainingApps);
  }

  return (
    <>
      <Toolbar disableGutters={true}>
        <Typography className={ classes.title } variant="h4" component="h1" gutterBottom>Apps</Typography>
        <Button variant="contained" color="primary" onClick={ () => { history.push('/apps/new') } }>New app</Button>
      </Toolbar>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell align="right">Version</TableCell>
              <TableCell align="right">Minimum Version</TableCell>
              <TableCell align="right">App Url</TableCell>
              <TableCell align="right">App Creator</TableCell>
              <TableCell align="right">Platform</TableCell>
              <TableCell align="right">Profiles</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {apps.map(app => (
              <TableRow hover key={app.id} style={{ cursor: 'pointer' }} onClick={ () => history.push(`/apps/edit/${app.id}`) }>
                <TableCell><Avatar alt={app.name} src={app.icon} /></TableCell>
                <TableCell component="th" scope="row">
                  {app.name}
                </TableCell>
                <TableCell align="right">{app.version}</TableCell>
                <TableCell align="right">{app.minimumVersion}</TableCell>
                <TableCell align="right">{app.appUrl}</TableCell>
                <TableCell align="right">{app.appCreator}</TableCell>
                <TableCell align="right">{app.platform}</TableCell>
                <TableCell align="right">{app.profiles}</TableCell>
                <TableCell align="right">
                  <IconButton aria-label="Edit" onClick={ () => history.push(`/apps/edit/${app.id}`) }>
                    <EditIcon />
                  </IconButton>
                  <IconButton aria-label="Delete" 
                    onClick={(e) => { 
                      e.stopPropagation();
                      removeApp(app.id); 
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default AppsPage;
