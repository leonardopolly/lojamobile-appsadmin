import React from 'react';
import { AppForm } from '~/components';

const AppInsertPage = () => {
  return ( 
    <AppForm />
  );
}
 
export default AppInsertPage;