export const xs = '30em';
export const sm = '48em';
export const md = '62em';
export const lg = '75em';
export const xl = '90em';