import React from 'react';
import { useParams, useHistory } from "react-router-dom";
import { Paper, TextField, Container, Button, FormControl, FormLabel, FormControlLabel, RadioGroup, MenuItem, Checkbox, ListItemText, Input, Select, InputLabel, Avatar, FormHelperText, Toolbar, IconButton, Typography } from '@material-ui/core';
import { Radio } from 'final-form-material-ui';
import { 
  AddPhotoAlternateOutlined as AddPhotoAlternateOutlinedIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons';
import { Form, Field } from 'react-final-form';
import Dropzone from "react-dropzone";
import clsx from "clsx";
import axios from 'axios';

import { useStyles } from "./styles";


const profiles = ['Profile1', 'Profile2'];


const AppForm = ({ initialValues = { 
  icon: '',
  name: '',
  version: '',
  minimumVersion: '',
  appUrl: '',
  appCreator: '',
  platform: '',
  profiles: [],
} }) => {
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();

  const handleSubmit = (values) => {
    const { icon, profiles, ...rest } = values;
    const payload = new FormData();

    if(icon.hasOwnProperty('file')) {
      payload.append('iconFile', icon.file, icon.file.name);
    }
    else {
      payload.append('icon', icon);
    }

    payload.append('profiles', profiles.join(','));
    
    Object.keys(rest).forEach((key) => {
      payload.append(key, rest[key]);
    });

    axios.post(id? `/api/apps/${id}` : '/api/apps', payload).then(() => {
      history.push('/')
    });

  }

  return ( 
    <Container maxWidth="sm">
      <Toolbar disableGutters={true}>
        <IconButton className={ classes.iconBack } onClick={ () => { history.goBack() } }>
          <ArrowBackIcon />
        </IconButton>
        <Typography variant="h5" component="h1" gutterBottom>{ (id && `Editing app "${initialValues.name}"`) || 'New app' }</Typography>
      </Toolbar>
      <Paper className={ classes.root }>
        <Form 
          onSubmit={ handleSubmit }
          initialValues={ initialValues }
        >
          {(formProps) => (
            <form onSubmit={formProps.handleSubmit}>

              <FormControl>
                <Field name="icon">
                  {(fieldprops) => {
                    const { input: { value, onChange, ...input } } = fieldprops;

                    const iconUrl = (value.dataUrl || (value && `/${value}`));

                    return (
                      <div className={ classes.iconInputWrapper }>
                        <Dropzone 
                          multiple={false}
                          onDrop={(files, e) => {
                            formProps.form.blur(`icon`);
                            
                            files.forEach((file) => {
                              const reader = new FileReader()
                              
                              reader.onabort = () => console.log('file reading was aborted')
                              reader.onerror = () => console.log('file reading has failed')
                              reader.onload = () => {
                                // Do whatever you want with the file contents
                                const dataUrl = reader.result;
                                formProps.form.change(`icon`, { file, dataUrl});
                                // setIconFile(binaryStr);
                              }
                              reader.readAsDataURL(file);
                              // reader.readAsArrayBuffer(file)
                            })
                          }}
                        >
                          {({getRootProps, getInputProps}) => (
                            <div {...getRootProps()} className={ clsx(classes.iconInput, value? null : `empty`) }>
                              <input {...getInputProps()} />
                              <Avatar src={ iconUrl }>
                                <AddPhotoAlternateOutlinedIcon />
                              </Avatar>
                              <FormHelperText variant="outlined" style={{ marginTop: '.3rem', }}>Drop or select icon</FormHelperText>
                            </div>
                          )}
                        </Dropzone>
                      </div>
                  )}}
                </Field>
              </FormControl>
              
              <Field name="name">
                {({ input }) => (
                  <TextField { ...input } label="Name" />
                )}
              </Field>
              <Field name="version">
                {({ input }) => (
                  <TextField { ...input } className={ classes.field50p } label="Version" />
                )}
              </Field>
              <Field name="minimumVersion">
                {({ input }) => (
                  <TextField { ...input } className={ classes.field50p } label="Minimum Version" />
                )}
              </Field>
              <Field name="appUrl">
                {({ input }) => (
                  <TextField { ...input } label="App Url" />
                )}
              </Field>
              <Field name="appCreator">
                {({ input }) => (
                  <TextField { ...input } label="App Creator" />
                )}
              </Field>
              <FormControl component="fieldset">
                <FormLabel component="legend">Platform</FormLabel>
                <RadioGroup row>
                  <FormControlLabel 
                    label="Android" 
                    control={
                      <Field 
                        name="platform"
                        value="Android"
                        component={Radio}
                        type="radio"
                      />
                    } 
                  />
                  <FormControlLabel 
                    label="iOS" 
                    control={
                      <Field 
                        name="platform"
                        value="iOS"
                        component={Radio}
                        type="radio"
                      />
                    } 
                  />
                </RadioGroup>
              </FormControl>
              <FormControl>
                <InputLabel id="profilesLabel">Profiles</InputLabel>
                <Field
                  name="profiles"
                  labelId="profilesLabel"
                >
                  { ({ input }) => (
                    <Select
                      multiple
                      { ...input }
                      input={<Input />}
                      renderValue={selected => selected.join(', ')}
                    >
                      {profiles.map(profile => (
                        <MenuItem key={profile} value={profile}>
                          <Checkbox checked={input.value.indexOf(profile) > -1} />
                          <ListItemText primary={profile} />
                        </MenuItem> 
                      ))}
                    </Select>
                  )}
                </Field>
              </FormControl>
              <Button variant="contained" color="primary" type="submit">Submit</Button> 
            </form>
          )}
        </Form>
      </Paper>
    </Container>
  );
}
 
export default AppForm;