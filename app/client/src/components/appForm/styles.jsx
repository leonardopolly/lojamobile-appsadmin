import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
  root: {
    padding: theme.spacing(5),
    '& .MuiFormControl-root': {
      width: `calc(100% - ${theme.spacing(3)}px)`,
      marginBottom: theme.spacing(3),
    },
    '& .MuiFormControl-root, & .MuiButtonBase-root': {
      marginLeft: theme.spacing(1.5),
      marginRight: theme.spacing(1.5),
    }
  },
  field50p: {
    '&.MuiFormControl-root': {
      width: `calc(50% - ${theme.spacing(3)}px)`,
    }
  },
  iconInputWrapper: {
    textAlign: 'center'
  },
  iconInput: {
    outline: 'none',
    display: 'inline-block',
    fontSize: 0,

    '& .MuiAvatar-root': {
      display: 'inline-flex',
      width: theme.spacing(11),
      height: theme.spacing(11),
      '& .MuiSvgIcon-root': {
        width: theme.spacing(4),
        height: theme.spacing(4),
      }
    },

    '&.empty .MuiAvatar-root': {
      width: theme.spacing(10),
      height: theme.spacing(10),
      overflow: 'visible',
      marginBottom: theme.spacing(1),
      '&:before': {
        content: '""',
        display: 'block',
        position: 'absolute',
        top: `-${theme.spacing(.5)}px`,
        left: `-${theme.spacing(.5)}px`,
        width: `calc(100% + ${theme.spacing(1)}px)`,
        height: `calc(100% + ${theme.spacing(1)}px)`,
        borderRadius: '50%',
        border: '2px dashed #b3b3b3',
      },
    },

    '&.empty:hover .MuiAvatar-root, &.empty:focus .MuiAvatar-root': {
      '&:before': {
        borderColor: '#3f51b5',
      },
    }
  },
  iconBack: {
    marginRight: theme.spacing(2)
  }
}));