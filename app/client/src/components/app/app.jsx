import React from "react";
import { Switch } from "react-router-dom";
import {
  Page,
  PrivatePage,
  PageNotFound,
  LoginPage,
  HomePage,
  AppsPage,
  AppEditPage,
  AppInsertPage
} from "~/pages";
import MainLayout from "~/components/mainLayout";
import { CssBaseline } from "@material-ui/core";

export const pages = [
  {
    title: "Home",
    path: "/",
    exact: true,
    showInMenu: false,
    component: HomePage
  },
  {
    title: "Apps",
    path: "/apps",
    exact: true,
    showInMenu: true,
    component: AppsPage
  },
  {
    title: "Edit App",
    path: "/apps/edit/:id",
    showInMenu: false,
    component: AppEditPage
  },
  {
    title: "New App",
    path: "/apps/new",
    showInMenu: false,
    component: AppInsertPage
  },
  {
    title: "Página não encontrada",
    path: "*",
    component: PageNotFound
  }
];

const App = () => {
  return (
    <>
      <CssBaseline />
      <Switch>
        <Page title="Login" path="/login" component={LoginPage} />
        <MainLayout>
          <Switch>
            {pages.map(page => (
              <PrivatePage
                title={page.title}
                key={page.path}
                exact={page.exact}
                path={page.path}
                component={page.component}
              />
            ))}
          </Switch>
        </MainLayout>
      </Switch>
    </>
  );
};

export default App;
