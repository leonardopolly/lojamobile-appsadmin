const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');
const multer  = require('multer');

const storage = multer.diskStorage({  
  destination: (req,file,cb) => {  
    cb(null,'./uploads')  
  },  
  filename: (req,file,cb) => {  
    const filename = path.normalize(file.originalname).replace(/^(\.\.(\/|\\|$))+/, '');
    cb(null,filename)  
    // check if file exists and do something
    // if (!fs.existsSync(path.join('./uploads',filename))) {
    // }
  }  
})  

const upload = multer({ storage });  

const db = require('./services/dbInterface')

const app = express(),
  port = 5000;

app.use(logger('dev'));
app.use(cors());
app.use( bodyParser.json() ); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
})); 

app.use(express.static(path.join(__dirname, 'client', 'build')));


app.get('/uploads/:filename', (req,res) => {
  res.sendFile(path.join(__dirname, 'uploads', req.params.filename))
});

app.get('/api/apps', (req,res) => {
  db.getApps(req,res);
  // res.send()
});
app.post('/api/apps', upload.single('iconFile'), (req, res) => {
  db.insertApp(req,res);
});

app.get('/api/apps/:id', (req, res) => {
  db.getApp(req,res);
});
app.post('/api/apps/:id', upload.single('iconFile'), (req, res) => {
  db.updateApp(req,res);
});
app.delete('/api/apps/:id', (req, res) => {
  db.deleteApp(req,res);
});

app.get('/*',(req,res) => {
  res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'))
});

// app.get('*', (req,res) => {
//   res.send({ message: 'Servidor backend ativo' })
// });

app.listen(port, () => console.log('Backend server live on port ' + port));